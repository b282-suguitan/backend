// console.log("Hello World!");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

	
// Initialize/add the given object properties and methods

// Properties

// Methods


	// Object literal declaration
let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ["Brock", "Misty"],
	},

	//Object method declaration
	talk: function() {
		return ("Pikachu! I choose you!");
	},

}
	
// Check if all properties and methods were properly added
	// Object trainer output
console.log(trainer);

// Access object properties using dot notation

	// access to property name using dot notation
console.log("Result of dot notations:");
console.log(trainer.name);

// Access object properties using square bracket notation

	// Acesss to property pokemon using square bracket notation
console.log("Result of square bracket notations:")
console.log(trainer['pokemon']);

// Access the trainer "talk" method
	
	// Access object method talk
console.log("Result of talk method");
console.log(trainer.talk());

// Create a constructor function called Pokemon for creating a pokemon

	// Object constructor declaration Pokemon
function Pokemon(name, level) {

	this.name = name;
	this.level = level; // 10
	this.health = 2 * level; // 20
	this.attack = level;

	// Object method declaration for pokemon attributes
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + Number(target.health -= this.attack));
		// Output the pokemon fainted if health drops below 0
		if(target.health <= 0){
			this.faint(target);
		}
	};	
	// Object method if pokemon fainted if health drops below 0
	this.faint = function(target) {
		console.log(target.name + ' fainted.');
	}
}

// Create/instantiate a new pokemon named pikachu

let pikachu = new Pokemon('Pikachu', 12);
console.log(pikachu);

// Create/instantiate a new pokemon named geodude

let geodude = new Pokemon('Geodude', 8);
console.log(geodude);

// Create/instantiate a new pokemon named mewtwo

let mewtwo = new Pokemon('Mewtwo', 100);
console.log(mewtwo);

// Invoke the tackle method and target pikachu

geodude.tackle(pikachu);

// Invoke the tackle method and target geodude

mewtwo.tackle(geodude);

// Output of geodude's current attributes
console.log(geodude);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}