// Create a server

// "require" directive used to load node.js modules
// "http" module that lets the node.js transfer data using the Hyper Text Transfer Protocol (HTTP)
let http = require("http");

// "createServer()" method used  to create an HTTP server that listens to requests on a specified port and give reponses back to the client
// "request" - messages sent by the client (usually a web browser)
// "response" - messages sent by the SERVER as an answer
http.createServer(function(request, response){

	// writeHead() method used to set a status code for response and set the content type of the response
	// 200 - successful request
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// end() method used to send the response with text content 'Hello World'
	response.end('Hello World!');

// .listen(portNumber)	method in node.js used to start a server listening for incoming connections on a specified port
// port = 4000
// kill port
	/*
	npx kill-port portNumber
	npx kill-port 4000
	*/
}).listen(4000)

console.log('Server running at localhost: 4000');